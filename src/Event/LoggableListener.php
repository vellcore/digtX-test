<?php

namespace App\Event;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

class LoggableListener extends \Gedmo\Loggable\LoggableListener
{
    private EntityManagerInterface $entityManager;
    private EventDispatcherInterface $dispatcher;
    private Security $user;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $user,
        EventDispatcherInterface $dispatcher,
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->dispatcher = $dispatcher;
    }

    public function prePersistLogEntry($logEntry, $object)
    {
        $username = $this->user->getUser()->getSystemUser()->getEmail();
        $logEntry->setUsername($username);

        $event = new EntityLogEvent($object, $logEntry, $this->entityManager);
        if ($logEntry->getAction() === 'create') {
            $this->dispatcher->dispatch($event, EntityLogEvent::CREATE);
        } elseif ($logEntry->getAction() === 'remove') {
            $this->dispatcher->dispatch($event, EntityLogEvent::REMOVE);
        } elseif ($logEntry->getAction() === 'update') {
            $this->dispatcher->dispatch($event, EntityLogEvent::UPDATE);
        }
    }
}
