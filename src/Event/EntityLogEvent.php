<?php

namespace App\Event;

use App\Entity\Work\D04_Logistik\Invinit2\LogEntry;
use Doctrine\ORM\EntityManagerInterface;

class EntityLogEvent
{
    public const REMOVE = 'loggable.entity.removed';
    public const CREATE = 'loggable.entity.created';
    public const UPDATE = 'loggable.entity.updated';

    protected $entity;
    protected $logEntry;
    protected $entityManager;
    protected $loggableRepository;

    public function __construct($entity, $logEntry, EntityManagerInterface $entityManager)
    {
        $this->entity = $entity;
        $this->logEntry = $logEntry;
        $this->entityManager = $entityManager;
        $this->loggableRepository = $entityManager->getRepository(LogEntry::class);
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getLogEntry()
    {
        return $this->logEntry;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param int $version the amount of versions to go back
     * @return LogEntry|null
     */
    public function getLogVersionBefore(int $version = 1): ?LogEntry
    {
        $logEntry = $this->getLogEntry();

        return $this->loggableRepository->findOneBy([
            'objectId' => $logEntry->getObjectId(),
            'objectClass' => $logEntry->getObjectClass(),
            'version' => $logEntry->getVersion() - $version,
            ]);
    }
}
