<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class CountryFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $sql = $this->countries();

        /* @var EntityManagerInterface $manager */
        $manager->getConnection()->executeStatement($sql);

        $manager->flush();
    }

    private function countries(): string
    {
        return "INSERT INTO app_countries (NameEn, NameDe, ISO3166) VALUES
            ('Afghanistan','Afghanistan','AF'),
            ('Ãland Islands','Ãland-Inseln','AX'),
            ('Albania','Albanien','AL'),
            ('Algeria','Algerien','DZ'),
            ('American Samoa','Amerikanisch-Samoa','AS'),
            ('Andorra','Andorra','AD'),
            ('Angola','Angola','AO'),
            ('Anguilla','Anguilla','AI'),
            ('Antarctica','Antarktis','AQ'),
            ('Antigua and Barbuda','Antigua und Barbuda','AG'),
            ('Argentina','Argentinien','AR'),
            ('Armenia','Armenien','AM'),
            ('Aruba','Aruba','AW'),
            ('Australia','Australien','AU'),
            ('Austria','Österreich','AT'),
            ('Azerbaijan','Aserbaidschan','AZ'),
            ('Bahamas','Bahamas','BS'),
            ('Bahrain','Bahrain','BH'),
            ('Bangladesh','Bangladesch','BD'),
            ('Barbados','Barbados','BB'),
            ('Belarus','Weißrussland','BY'),
            ('Belgium','Belgien','BE'),
            ('Belize','Belize','BZ'),
            ('Benin','Benin','BJ'),
            ('Bermuda','Bermuda','BM'),
            ('Bhutan','Bhutan','BT'),
            ('Bolivia, Plurinational State of','Bolivien Plurinationaler Staat','BO'),
            ('Bonaire, Sint Eustatius and Saba','Bonaire Sint Eustatius und Saba','BQ'),
            ('Bosnia and Herzegovina','Bosnien und Herzegowina','BA'),
            ('Botswana','Botswana','BW'),
            ('Bouvet Island','Bouvetinsel','BV'),
            ('Brazil','Brasilien','BR'),
            ('British Indian Ocean Territory','Britisches Territorium im Indischen Ozean','IO'),
            ('Brunei Darussalam','Brunei Darussalam','BN'),
            ('Bulgaria','Bulgarien','BG'),
            ('Burkina Faso','Burkina Faso','BF'),
            ('Burundi','Burundi','BI'),
            ('Cambodia','Kambodscha','KH'),
            ('Cameroon','Kamerun','CM'),
            ('Canada','Kanada','CA'),
            ('Cape Verde','Kap Verde','CV'),
            ('Cayman Islands','Cayman-Inseln','KY'),
            ('Central African Republic','Zentralafrikanische Republik','CF'),
            ('Chad','Tschad','TD'),
            ('Chile','Chile','CL'),
            ('China','China','CN'),
            ('Christmas Island','Weihnachtsinsel','CX'),
            ('Cocos (Keeling) Islands','Kokosinseln (Keeling)','CC'),
            ('Colombia','Kolumbien','CO'),
            ('Comoros','Komoren','KM'),
            ('Congo','Kongo','CG'),
            ('Congo, the Democratic Republic of the','Kongo die Demokratische Republik','CD'),
            ('Cook Islands','Cook-Inseln','CK'),
            ('Costa Rica','Costa Rica','CR'),
            ('CÃ´te d''Ivoire','CÃ''te d''Ivoire','CI'),
            ('Croatia','Kroatien','HR'),
            ('Cuba','Kuba','CU'),
            ('CuraÃ§ao','CuraÃ§ao','CW'),
            ('Cyprus','Zypern','CY'),
            ('Czech Republic','Tschechische Republik','CZ'),
            ('Denmark','Dänemark','DK'),
            ('Djibouti','Dschibuti','DJ'),
            ('Dominica','Dominica','DM'),
            ('Dominican Republic','Dominikanische Republik','DO'),
            ('Ecuador','Ecuador','EC'),
            ('Egypt','Ägypten','EG'),
            ('El Salvador','El Salvador','SV'),
            ('Equatorial Guinea','Äquatorialguinea','GQ'),
            ('Eritrea','Eritrea','ER'),
            ('Estonia','Estland','EE'),
            ('Ethiopia','Äthiopien','ET'),
            ('Falkland Islands (Malvinas)','Falklandinseln (Malwinen)','FK'),
            ('Faroe Islands','Färöer Inseln','FO'),
            ('Fiji','Fidschi','FJ'),
            ('Finland','Finnland','FI'),
            ('France','Frankreich','FR'),
            ('French Guiana','Französisch-Guayana','GF'),
            ('French Polynesia','Französisch-Polynesien','PF'),
            ('French Southern Territories','Französische Südterritorien','TF'),
            ('Gabon','Gabun','GA'),
            ('Gambia','Gambia','GM'),
            ('Georgia','Georgien','GE'),
            ('Germany','Deutschland','DE'),
            ('Ghana','Ghana','GH'),
            ('Gibraltar','Gibraltar','GI'),
            ('Greece','Griechenland','GR'),
            ('Greenland','Grönland','GL'),
            ('Grenada','Grenada','GD'),
            ('Guadeloupe','Guadeloupe','GP'),
            ('Guam','Guam','GU'),
            ('Guatemala','Guatemala','GT'),
            ('Guernsey','Guernsey','GG'),
            ('Guinea','Guinea','GN'),
            ('Guinea-Bissau','Guinea-Bissau','GW'),
            ('Guyana','Guyana','GY'),
            ('Haiti','Haiti','HT'),
            ('Heard Island and McDonald Islands','Heard-Insel und McDonald-Inseln','HM'),
            ('Holy See (Vatican City State)','Heiliger Stuhl (Staat Vatikanstadt)','VA'),
            ('Honduras','Honduras','HN'),
            ('Hong Kong','Hongkong','HK'),
            ('Hungary','Ungarn','HU'),
            ('Iceland','Island','IS'),
            ('India','Indien','IN'),
            ('Indonesia','Indonesien','ID'),
            ('Iran, Islamic Republic of','Iran Islamische Republik','IR'),
            ('Iraq','Irak','IQ'),
            ('Ireland','Irland','IE'),
            ('Isle of Man','Isle of Man','IM'),
            ('Israel','Israel','IL'),
            ('Italy','Italien','IT'),
            ('Jamaica','Jamaika','JM'),
            ('Japan','Japan','JP'),
            ('Jersey','Jersey','JE'),
            ('Jordan','Jordanien','JO'),
            ('Kazakhstan','Kasachstan','KZ'),
            ('Kenya','Kenia','KE'),
            ('Kiribati','Kiribati','KI'),
            ('Korea, Democratic People''s Republic of','Korea Demokratische Volksrepublik','KP'),
            ('Korea, Republic of','Korea Republik','KR'),
            ('Kuwait','Kuwait','KW'),
            ('Kyrgyzstan','Kirgisistan','KG'),
            ('Lao People''s Democratic Republic','Demokratische Volksrepublik Laos','LA'),
            ('Latvia','Lettland','LV'),
            ('Lebanon','Libanon','LB'),
            ('Lesotho','Lesotho','LS'),
            ('Liberia','Liberia','LR'),
            ('Libya','Libyen','LY'),
            ('Liechtenstein','Liechtenstein','LI'),
            ('Lithuania','Litauen','LT'),
            ('Luxembourg','Luxemburg','LU'),
            ('Macao','Macao','MO'),
            ('Macedonia, the Former Yugoslav Republic of','Mazedonien die ehemalige jugoslawische Republik','MK'),
            ('Madagascar','Madagaskar','MG'),
            ('Malawi','Malawi','MW'),
            ('Malaysia','Malaysia','MY'),
            ('Maldives','Malediven','MV'),
            ('Mali','Mali','ML'),
            ('Malta','Malta','MT'),
            ('Marshall Islands','Marshall-Inseln','MH'),
            ('Martinique','Martinique','MQ'),
            ('Mauritania','Mauretanien','MR'),
            ('Mauritius','Mauretanien','MU'),
            ('Mayotte','Mayotte','YT'),
            ('Mexico','Mexiko','MX'),
            ('Micronesia, Federated States of','Mikronesien Föderierte Staaten von','FM'),
            ('Moldova, Republic of','Moldawien Republik','MD'),
            ('Monaco','Monaco','MC'),
            ('Mongolia','Mongolei','MN'),
            ('Montenegro','Montenegro','ME'),
            ('Montserrat','Montserrat','MS'),
            ('Morocco','Marokko','MA'),
            ('Mozambique','Mosambik','MZ'),
            ('Myanmar','Myanmar','MM'),
            ('Namibia','Namibia','NA'),
            ('Nauru','Nauru','NR'),
            ('Nepal','Nepal','NP'),
            ('Netherlands','Niederlande','NL'),
            ('New Caledonia','Neukaledonien','NC'),
            ('New Zealand','neuseeland','NZ'),
            ('Nicaragua','Nicaragua','NI'),
            ('Niger','Niger','NE'),
            ('Nigeria','Nigeria','NG'),
            ('Niue','Niue','NU'),
            ('Norfolk Island','Norfolkinsel','NF'),
            ('Northern Mariana Islands','Nördliche Marianen','MP'),
            ('Norway','Norwegen','NO'),
            ('Oman','Oman','OM'),
            ('Pakistan','Pakistan','PK'),
            ('Palau','Palau','PW'),
            ('Palestine, State of','Palästina Staat','PS'),
            ('Panama','Panama','PA'),
            ('Papua New Guinea','Papua-Neuguinea','PG'),
            ('Paraguay','Paraguay','PY'),
            ('Peru','Peru','PE'),
            ('Philippines','Philippinen','PH'),
            ('Pitcairn','Pitcairn','PN'),
            ('Poland','Polen','PL'),
            ('Portugal','Portugal','PT'),
            ('Puerto Rico','Puerto Rico','PR'),
            ('Qatar','Katar','QA'),
            ('RÃ©union','RÃ©union','RE'),
            ('Romania','Rumänien','RO'),
            ('Russian Federation','Russische Föderation','RU'),
            ('Rwanda','Ruanda','RW'),
            ('Saint BarthÃ©lemy','St. BarthÃ©lemy','BL'),
            ('Saint Helena, Ascension and Tristan da Cunha','St. Helena Ascension und Tristan da Cunha','SH'),
            ('Saint Kitts and Nevis','St. Kitts und Nevis','KN'),
            ('Saint Lucia','St. Lucia','LC'),
            ('Saint Martin (French part)','St. Martin (französischer Teil)','MF'),
            ('Saint Pierre and Miquelon','St. Pierre und Miquelon','PM'),
            ('Saint Vincent and the Grenadines','St. Vincent und die Grenadinen','VC'),
            ('Samoa','Samoa','WS'),
            ('San Marino','San Marino','SM'),
            ('Sao Tome and Principe','Sao Tome und Principe','ST'),
            ('Saudi Arabia','Saudi-Arabien','SA'),
            ('Senegal','Senegal','SN'),
            ('Serbia','Serbien','RS'),
            ('Seychelles','Seychellen','SC'),
            ('Sierra Leone','Sierra Leone','SL'),
            ('Singapore','Singapur','SG'),
            ('Sint Maarten (Dutch part)','Sint Maarten (Niederländischer Teil)','SX'),
            ('Slovakia','Slowakei','SK'),
            ('Slovenia','Slowenien','SI'),
            ('Solomon Islands','Salomon-Inseln','SB'),
            ('Somalia','Somalia','SO'),
            ('South Africa','Südafrika','ZA'),
            ('South Georgia and the South Sandwich Islands','Südgeorgien und die Südlichen Sandwichinseln','GS'),
            ('South Sudan','Südsudan','SS'),
            ('Spain','Spanien','ES'),
            ('Sri Lanka','Sri Lanka','LK'),
            ('Sudan','Sudan','SD'),
            ('Suriname','Surinam','SR'),
            ('Svalbard and Jan Mayen','Svalbard und Jan Mayen','SJ'),
            ('Swaziland','Swasiland','SZ'),
            ('Sweden','Schweden','SE'),
            ('Switzerland','Schweiz','CH'),
            ('Syrian Arab Republic','Arabische Republik Syrien','SY'),
            ('Taiwan, Province of China','Taiwan Provinz China','TW'),
            ('Tajikistan','Tadschikistan','TJ'),
            ('Tanzania, United Republic of','Tansania Vereinigte Republik','TZ'),
            ('Thailand','Thailand','TH'),
            ('Timor-Leste','Timor-Leste','TL'),
            ('Togo','Togo','TG'),
            ('Tokelau','Tokelau','TK'),
            ('Tonga','Tonga','TO'),
            ('Trinidad and Tobago','Trinidad und Tobago','TT'),
            ('Tunisia','Tunesien','TN'),
            ('Turkey','Türkei','TR'),
            ('Turkmenistan','Turkmenistan','TM'),
            ('Turks and Caicos Islands','Turks- und Caicosinseln','TC'),
            ('Tuvalu','Tuvalu','TV'),
            ('Uganda','Uganda','UG'),
            ('Ukraine','Ukraine','UA'),
            ('United Arab Emirates','Vereinigte Arabische Emirate','AE'),
            ('United Kingdom','Vereinigtes Königreich','GB'),
            ('United States','Vereinigte Staaten','US'),
            ('United States Minor Outlying Islands','Vereinigte Staaten Minor Outlying Islands','UM'),
            ('Uruguay','Uruguay','UY'),
            ('Uzbekistan','Usbekistan','UZ'),
            ('Vanuatu','Vanuatu','VU'),
            ('Venezuela, Bolivarian Republic of','Venezuela Bolivarische Republik','VE'),
            ('Viet Nam','Vietnam','VN'),
            ('Virgin Islands, British','Jungferninseln Britische','VG'),
            ('Virgin Islands, U.S.','Jungferninseln U.S.','VI'),
            ('Wallis and Futuna','Wallis und Futuna','WF'),
            ('Western Sahara','Westsahara','EH'),
            ('Yemen','Jemen','YE'),
            ('Zambia','Sambia','ZM'),
            ('Zimbabwe','Simbabwe','ZW');";
    }
}