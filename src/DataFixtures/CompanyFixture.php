<?php

namespace App\DataFixtures;

use App\Entity\Admin\Company\Company;
use App\Form\Model\UserDetail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CompanyFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $company = new Company();
        $company->setStatus(UserDetail::STATUS_DEFAULT);
        $company->setEmail($faker->email);
        $company->setLogo($faker->imageUrl());
        $company->setName($faker->name);
        $company->setSlackTeam('T01HPFCP0RG');

        $manager->persist($company);
        $manager->flush();
    }
}