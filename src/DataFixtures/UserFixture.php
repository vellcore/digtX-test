<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserAuth;
use App\Form\Model\UserDetail;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixture extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
    ) {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $user = new User();
        $user->setStatus(UserDetail::STATUS_DEFAULT);
        $user->setEmail('test.user');
        $user->setName('Test');
        $user->setUsername('Test User');
        $user->setLastName('User');

        $manager->persist($user);

        $auth = new UserAuth();
        $auth->setEmail('test.user');
        $auth->setPassword($this->passwordHasher->hashPassword($auth, 'test'));
        $auth->setSystemUser($user);

        $manager->persist($auth);

        for ($i = 0; $i < 100; $i++) {
            $user = new User();

            $user->setEmail($faker->email);
            $user->setName($faker->name);
            $user->setUsername($faker->userName);
            $user->setLastName($faker->lastName);
            $user->setStatus(UserDetail::STATUS_DEFAULT);

            $manager->persist($user);
        }


        $manager->flush();
    }
}

