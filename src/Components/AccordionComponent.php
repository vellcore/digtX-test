<?php

namespace App\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('accordion')]
class AccordionComponent
{
    public string $title;
    public array $panels;
}
