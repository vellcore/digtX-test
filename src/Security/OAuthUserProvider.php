<?php

namespace App\Security;

use App\Entity\User;
use App\Entity\UserAuth;
use App\Repository\UserAuthRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use http\Exception\InvalidArgumentException;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class OAuthUserProvider implements OAuthAwareUserProviderInterface
{
    private UserAuthRepository $userAuthRepository;
    private UserPasswordHasherInterface $passwordHasher;
    private EntityManagerInterface $entityManager;
    private MailerInterface $mailer;

    public function __construct(
        UserAuthRepository $userAuthRepository,
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $entityManager,
        MailerInterface $mailer,
    ) {
        $this->userAuthRepository = $userAuthRepository;
        $this->passwordHasher = $passwordHasher;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response): UserAuth
    {
        $data = $response->getData();
        $user = $this->userAuthRepository->findOneBy(['email' => $data['email']]);

        try {
            if ($user === null) {
                $systemUser = new User();
                $user = new UserAuth();

                $systemUser->setEmail($data['email']);
                $systemUser->setName($data['given_name']);
                $systemUser->setLastName($data['family_name']);
                $systemUser->setUsername($data['name']);

                $user->setEmail($data['email']);
                $user->setRoles(["ROLE_USER"]);
                $user->setIsVerified($data['verified_email']);
                $user->setSystemUser($systemUser);
                $user = $this->generatePassword($user);

                $this->entityManager->persist($user);
                $this->entityManager->persist($systemUser);

                $this->entityManager->flush();
            }
        } catch (Exception $exception) {
            throw new AuthenticationException($exception);
        }

        return $user;
    }

    /**
     * Generates cryptographically secure password with
     * given string $length and by default sends email
     * with credentials to the user.
     *
     * @param UserAuth $user
     * @param int $length
     * @param bool $sendMail
     * @return UserAuth
     * @throws Exception
     */
    public function generatePassword(UserAuth $user, int $length = 12, bool $sendMail = true): UserAuth
    {
        if ($length < 6) {
            throw new InvalidArgumentException("Password must be at least 6 characters long!");
        }

        $chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ1234567890";
        $specialChars = "!@#$%^&*()_+";
        $charLength = mb_strlen($chars, '8bit') - 1;
        $specialCharsLenght = mb_strlen($specialChars, '8bit') - 1;

        // generate random Password
        $plainPassword = "";
        for ($i = 0; $i < $length; $i++) {
            $plainPassword .= $chars[random_int(0, $charLength)];
        }

        // add ~20% special chars
        for ($j = 0; $j < $length / 5; $j++) {
            $plainPassword[random_int(0, $length - 1)] = $specialChars[random_int(0, $specialCharsLenght)];
        }

        $user->setPassword($this->passwordHasher->hashPassword($user, $plainPassword));

        //@TODO: Send email with password (configure mail sender)
        if (true) {
            return $user;
        }

        try {
            $email = (new Email())
                ->from('system@digtx.ch')
                ->to($user->getEmail())
                ->subject('DigtX 2.0 Registration')
                ->text("You've successfully registered on DigtX 2.0! 
                    Since you have registered with your Google account,
                     we have generated a password for you: $plainPassword");

            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new AuthenticationException("Could not send email");
        }

        return $user;
    }
}
