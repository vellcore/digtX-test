<?php

namespace App\Form\Type;

use App\Service\Helper\ArrayHelper;
use App\Service\ListViewPaginator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class ListViewType extends AbstractType
{
    private Request $request;
    private Session $session;

    private array $paginators;
    private string $sessionKey;

    public function __construct(
        RequestStack $request,
    ) {
        $this->request = $request->getCurrentRequest();
        $this->session = $request->getSession();
        $this->paginators = [];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->assertOptions($options);
        $form = $builder->getForm();

        $listName = $builder->getForm()->getRoot()?->getName();
        $this->paginators[$listName] = new ListViewPaginator();

        $params = $this->request->request->all();

        $this->sessionKey = "list-view-{$form->getName()}-parameters";
        $sessionParams = unserialize($this->session->get("list-view-{$form->getName()}-parameters"));

        $newParams = [];
        foreach ($params as $key => $param) {
            // filter out params from other forms on the same page
            if (!str_starts_with($key, $form->getName()) && ! is_array($param)) {
                continue;
            }

            $newParams[$key] = $param;
        }

        if ($newParams === [] && $sessionParams !== false) {
            $newParams = $sessionParams;
        }

        $data = [];
        foreach ($newParams as $key => $param) {
            // filter out params that are not filter data
            if (str_starts_with($key, $form->getName() . 'list_view') || ! is_array($param)) {
                continue;
            }

            $data = $param;
        }

        // add configured filters to form
        $filters = $options['filters'];
        foreach ($filters as $filter) {
            $class = new $filter['type']();
            if (! $class instanceof AbstractFilterType) {
                throw new UnexpectedTypeException($class, AbstractFilterType::class);
            }
            $filter['options']['required'] = false;

            if (isset($data[$listName]) && isset($data[$listName][$filter['name']])) {
                $filter['options']['data'] = $data[$listName][$filter['name']];
                // set default value if filter is empty
            } elseif ($newParams === [] && $sessionParams === false && isset($filter['options']['default_value'])) {
                $filter['options']['data'] = array_values(ArrayHelper::flattenArray($filter['options']['default_value']));
                $newParams = $filter['options']['default_value'];
            }

            $builder->add($filter['name'], $filter['type'], $filter['options']);
        }

        $pageLimiter = $newParams[$form->getName() . '_list_view_page_limiter']
            ?? $options['pagination']['limits']['default'];

        $newParams[$form->getName() . '_list_view_page_limiter'] = $pageLimiter;

        // build paginator service with provided queryBuilder
        $this->paginators[$listName]->build($options['queryBuilder'], $newParams, $options, $form);

        // override form data with dynamic data set
        $builder->setData($this->paginators[$listName]->getData());

        $this->session->set("list-view-{$form->getName()}-parameters", serialize($newParams));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $params = $this->request->request->all();

        $sessionParams = unserialize($this->session->get("list-view-{$form->getName()}-parameters"));

        if ($params === []) {
            $params = $sessionParams;
        }

        foreach ($options as $key => $option) {
            $view->vars[$key] = $option;
        }

        $pageLimiter = $params[$form->getName() . '_list_view_page_limiter']
            ?? $options['pagination']['limits']['default'];
        $view->vars['list_view_page_limiter'] = $pageLimiter;
        $view->vars['list_view_search'] = $params[$form->getName() . '_list_view_search'] ?? '';
        $view->vars['list_view_page_count'] = $this->paginators[$form->getName()]->getPageCount();
        $view->vars['list_view_current_page'] = $this->paginators[$form->getName()]->getCurrentPage();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'filters' => [],
            'search_fields' => [],
            'queryBuilder' => null,
            'columns' => [
                'id' => [],
                'name' => [],
                'date' => [],
            ],
            'allow_add' => true,
            'allow_edit' => true,
            'allow_remove' => false,
            'allow_search' => true,
            'allow_limit' => true,
            'label' => false,
            'turbo_src' => null,
            'cardboard' => true,
            'title' => 'List View',
            'add_btn_label' => 'Add +',
            'add_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'edit_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'remove_btn_path' => [
                'name' => null,
                'properties' => [
                    'entityId' => 'id',
                ]
            ],
            'custom_actions' => [],
            'pagination' => [
                'limits' => [
                    'list' => [
                        '5',
                        '10',
                        '25',
                        '50',
                        '100',
                    ],
                    'default' => '50',
                ],
            ],
        ]);
    }

    private function assertOptions(array $options): void
    {
        if ($options['allow_add'] && $options['add_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'add_btn_path': Provide 'add_btn_path' or set 'allow_add' to false",
                $options
            );
        }

        if ($options['allow_edit'] && $options['edit_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'edit_btn_path': Provide 'edit_btn_path' or set 'allow_edit' to false",
                $options
            );
        }

        if ($options['allow_remove'] && $options['remove_btn_path']['name'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'remove_btn_path': Provide 'remove_btn_path' or set 'allow_remove' to false",
                $options
            );
        }

        if ($options['queryBuilder'] === null) {
            throw new MissingOptionsException(
                "Missing Argument 'queryBuilder': Unable to show data",
                $options
            );
        }
    }
}
