<?php

namespace App\Form\Type;

use App\Form\Admin\Mapper\MultiselectToForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultiselectType extends AbstractType
{
    private EntityManagerInterface $entityManager;
    private array $objectRepositories;
    private Session $session;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack,
    ) {
        $this->entityManager = $entityManager;
        $this->session = $requestStack->getSession();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['entity'] === null) {
            throw new MissingOptionsException();
        }

        $builder
            ->add('multiselect', TextType::class, [
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Search...',
                ],
                'label' => $options['label'],
                "autocomplete" => true,
                'tom_select_options' => [
                    'maxItems' => null,
                    'hideSelected' => true,
                    'copyClassesToDropdown' => false,
                    'maxOptions' => null,
                ],
            ]);

        if ($options['entity'] !== false) {
            $this->objectRepositories[$options['entity']] = $this->entityManager->getRepository($options['entity']);
        }

        $builder->setDataMapper(new MultiselectToForm($this->entityManager));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['action'] = $options['action'];
        $view->vars['row_attr'] = $options['row_attr'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'dropdown_label' => ['id'],
            'data_url' => '',
            'entity' => null,
            'tom_select_options' => [],
            'default_value' => null,
        ]);
    }
}
