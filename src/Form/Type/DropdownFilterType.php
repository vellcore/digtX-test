<?php

namespace App\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DropdownFilterType extends AbstractFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dropdownFilter', AutocompleteDropdownType::class, [
            'entity' => $options['entity'],
            'label' => $options['label'],
            'action' => $options['action'],
            'search_label' => 'list_view_q',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'entity' => null,
        ]);
    }
}