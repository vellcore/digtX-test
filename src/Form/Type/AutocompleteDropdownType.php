<?php

namespace App\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AutocompleteDropdownType extends AbstractType
{
    private EntityManagerInterface $entityManager;
    private array $objectRepositories;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formName = $builder->getForm()->getName();
        $this->objectRepositories[$formName] = $this->entityManager->getRepository($options['entity']);

        if (empty($options['entity'])) {
            throw new MissingOptionsException();
        }

         $builder
            ->add('autocompleteDropdown', TextType::class, [
                'mapped' => false,
                'attr' => [
                    'placeholder' => 'Search...',
                ],
                'label' => $options['label'],
            ])
             ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
                 $data = $event->getData();
                 $form = $event->getForm();
             })
             ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();

                if ($data === null) {
                    $form->setData($data);
                    return;
                }
                $data = $this->objectRepositories[$form->getName()]->find($data['autocompleteDropdown']);

                $form->setData($data);
         });
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['action'] = $options['action'];
        $view->vars['row_attr'] = $options['row_attr'];
        $view->vars['search_label'] = $options['search_label'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'dropdown_label' => ['id'],
            'data_url' => '',
            'entity' => null,
            'search_label' => 'q',
        ]);
    }
}
