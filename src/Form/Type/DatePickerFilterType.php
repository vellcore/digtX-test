<?php

namespace App\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

class DatePickerFilterType extends AbstractFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('datepickerFilterFrom', DatePickerType::class, [
            'label' => $options['label'],
        ]);
        $builder->add('datepickerFilterTo', DatePickerType::class, [
            'label' => false,
        ]);
    }
}