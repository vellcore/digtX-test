<?php

namespace App\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultiselectFilterType extends AbstractFilterType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('multiselectFilter', MultiselectType::class, [
            'entity' => $options['entity'],
            'label' => $options['label'],
            'action' => $options['action'],
            'data' => $options['data'] ?? [],
            'block_name' => $options['name'] ?? [],
            'tom_select_options' => $options['tom_select_options'],
            'default_value' => $options['default_value'] ?? null,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'entity' => null,
            'tom_select_options' => [],
            'default_value' => null,
        ]);
    }
}