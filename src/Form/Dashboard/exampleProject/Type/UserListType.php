<?php

namespace App\Form\Dashboard\exampleProject\Type;

use App\Entity\User;
use App\Form\Type\DatePickerFilterType;
use App\Form\Type\DropdownFilterType;
use App\Form\Type\ListViewType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class UserListType extends AbstractType
{
    private Request $request;
    private UrlGeneratorInterface $urlGenerator;

    private ObjectRepository $userRepository;
    private Environment $twig;

    private $modal;

    public function __construct(
        RequestStack $request,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        Environment $twig,
    ) {
        $this->request = $request->getCurrentRequest();
        $this->urlGenerator = $urlGenerator;
        $this->twig = $twig;

        $this->userRepository = $entityManager->getRepository(User::class);
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $this->userRepository->getUserList();

        $this->modal = $this->twig->load("layout/_modal.html.twig");

        $builder
            ->add('users', ListViewType::class, [
                // These are the necessary options for a basic list view
                'allow_add' => false,
                'allow_edit' => true,
                // provide the QueryBuilder which gets extended by search
                // and filter queries based on the configured options
                'queryBuilder' => $qb,
                'columns' => [
                    // a column can look like this, where the key is the
                    // data column and the value is the label for the view
                    'name' => 'Name',
                    // or like this, where additional parameters like "width" or a custom "callback" function can
                    // be defined
                    'lastName' => [
                        'name' => 'Last name',
                        'width' => '50%',
                        'callback' => function ($args) {
                            // the data provided from the queryBuilder
                            $data = $args['data'];
                            // the form interface
                            $form = $args['form'];

                            // modify / return the data
                            return "<span style='color: red'>$data[lastName]</span>";
                        }
                    ],
                    'email' => 'Email',
                ],
                'search_fields' => [
                    'u.name',
                    'u.lastName',
                    'u.email',
                ],
                'filters' => [
                    // Here any *FilterType can be configured to filter the ListView
                    [
                        'type' => DropdownFilterType::class,
                        // form name
                        'name' => 'User',
                        // the field on which the filter should be applied
                        'field' => 'u.id',
                        'options' => [
                            // necessary to apply the filter correctly
                            'entity' => User::class,
                            // custom action to provide the filter entries
                            'action' => $this->urlGenerator->generate('dropdown_get_users'),
                            'label' => 'User',
                            'width' => '80%',
                        ],
                    ],
                    [
                        'type' => DatePickerFilterType::class,
                        'name' => 'Date',
                        'field' => 'u.created',
                        'options' => [
                            'label' => 'Date',
                            'width' => '20%',
                        ],
                    ],
                ],
                'edit_btn_path' => [
                    'name' => 'work_digt_ag_product_edit',
                    'properties' => [
                        'productId' => 'id', // id needs to be a property of the entity or a provided column from queryBuilder DQL
                    ],
                ],
                'custom_actions' => [
                    // custom actions can be used to provide other actions than edit or add
                    [
                        // route name
                        'path' => 'home',
                        // Button name
                        'btn_name' => 'Home',
                        // Css classes
                        'class' => 'p-btn',
                        // route properties
                        'properties' => [
                            'knowledgeId' => 'id',
                        ],
                    ],
                    // for more freedom also callbacks can be used. f.e. this is useful for modals:
                    [
                        'callback' => function ($args) {
                            $url = $this->urlGenerator->generate('content');

                            return '<div data-controller="modal" data-modal-form-url-value="' . $url . '">
                                        <a class="p-btn" data-action="modal#openModal">Modal</a>
                                        ' . $this->modal->render([
                                            'modalData' => [
                                                'title' => $args['data']['lastName'],
                                            ],
                                        ]) . '
                                    </div>';
                        }
                    ]
                ]
            ]);
    }
}