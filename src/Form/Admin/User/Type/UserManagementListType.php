<?php

namespace App\Form\Admin\User\Type;

use App\Entity\Admin\Company\Company;
use App\Entity\User;
use App\Form\Type\DropdownFilterType;
use App\Form\Type\ListViewType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserManagementListType extends AbstractType
{
    private ObjectRepository $userRepository;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
    ) {
        $this->urlGenerator = $urlGenerator;

        $this->userRepository = $entityManager->getRepository(User::class);
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $this->userRepository->getUserManagementList();

        $builder
            ->add('users', ListViewType::class, [
                'queryBuilder' => $qb,
                'title' => false,
                'columns' => [
                    'digtUId' => 'Digt UID',
                    'status' => 'Status',
                    'name' => 'Name',
                    'lastName' => 'Last Name',
                    'email' => 'Email',
                ],
                'search_fields' => [
                    'u.digtUId',
                    'u.name',
                    'u.lastName',
                    'u.email',
                    'companyNames',
                ],
                'filters' => [
                ],
                'edit_btn_path' => [
                    'name' => 'app_admin_user_management_edit_detail',
                    'properties' => [
                        'userId' => 'id',
                    ],
                ],
                'add_btn_path' => [
                    'name' => 'app_admin_user_management_edit_detail',
                ],
            ]);
    }
}
