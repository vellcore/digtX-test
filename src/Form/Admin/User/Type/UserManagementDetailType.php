<?php

namespace App\Form\Admin\User\Type;

use App\Entity\Country\Country;
use App\Form\Type\StatusToggleType;
use App\Service\Company\Manager\CompanyManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class UserManagementDetailType extends AbstractType
{
    private CompanyManager $companyManager;

    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['data'];

        if ($data->getDigtUId() === null) {
            $digtUIdRequired = true;
            $digtUIdDisabled = false;
        } else {
            $digtUIdRequired = false;
            $digtUIdDisabled = true;
        }

        if ($data->getEmail() === null) {
            $emailRequired = true;
            $emailDisabled = false;
        } else {
            $emailRequired = false;
            $emailDisabled = true;
        }

        $builder
            ->add('digtUId', NumberType::class, [
                'label' => 'Personnel Nr. (Digt UID)',
                'disabled' => $digtUIdDisabled,
                'required' => $digtUIdRequired,
            ])
            ->add('status', StatusToggleType::class)
            ->add('name', TextType::class)
            ->add('lastName', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'male' => 'male',
                    'female' => 'female',
                    'non-binary' => 'non-binary',
                ]
            ])
            ->add('phoneNumber', TelType::class, [
                'required' => false,
            ])
            ->add('phoneNumberMobile', TelType::class, [
                'required' => false,
            ])
            ->add('profilePicture', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image type',
                    ])
                ],
                'row_attr' => [
                    'hidden'
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email / Login',
                'disabled' => $emailDisabled,
                'required' => $emailRequired,
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
            ])
            ->add('birthDate', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => 'disabled'
                ]
            ])
            ->add('hireDate', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => 'disabled'
                ]
            ])
            ->add('jobDescription', TextType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 80
                ]
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 100
                ]
            ])
            ->add('plz', NumberType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 11
                ]
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 80
                ]
            ])
            ->add('country', EntityType::class, [
                'required' => false,
                'choice_label' => function (Country $country) {
                    return $country->getNameEn();
                },
                'class' => Country::class
            ])
            ->add('phoneNumber', TelType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 20
                ]
            ])
            ->add('phoneNumberMobile', TelType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 20
                ]
            ])
            ->add('slackTeamId', ChoiceType::class, [
                'required' => false,
                'choices' => $this->companyManager->getSlackTeamsList()
            ])
            ->add('slackUserId', TextType::class, [
                'required' => false,
                'attr' => [
                    'maxlength' => 80
                ]
            ])
        ;
    }
}
