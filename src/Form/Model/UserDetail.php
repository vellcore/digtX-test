<?php

namespace App\Form\Model;

use App\Entity\Country\Country;
use App\Entity\User;

class UserDetail
{
    public const STATUS_DEFAULT = 1;

    private ?int $status = 1;
    private ?string $name = "";
    private ?string $lastName = "";
    private ?string $gender = "";
    private ?string $phoneNumber = "";
    private ?string $phoneNumberMobile = "";
    private ?string $email = "";
    private ?string $profilePicture = "";
    private ?string $password = "";
    private ?string $birthDate = null;
    private ?string $hireDate = null;
    private ?string $jobDescription = null;
    private ?string $address = null;
    private ?int $plz = null;
    private ?string $city = null;
    private ?string $slackUserId = null;
    private ?string $slackTeamId = null;
    private ?Country $country = null;

    private ?int $digtUId = 0;
    private $companies = [];
    private array $roles = [];

    public function __construct(User $user)
    {
        $userDetails = $user->getDetails();

        $this->name = $user->getName();
        $this->lastName = $user->getLastName();
        $this->gender = $user->getGender();
        $this->status = $user->getStatus();
        $this->digtUId = $user->getDigtUId();
        $this->email = $user->getEmail();
        $this->profilePicture = $user->getProfilePicture();

        $this->birthDate = $userDetails?->getBirthDate() ? $userDetails->getBirthDate()->format('d.m.Y') : null;
        $this->hireDate = $userDetails?->getHireDate() ? $userDetails->getHireDate()->format('d.m.Y') : null;
        $this->phoneNumber = $userDetails?->getPhoneNumber();
        $this->phoneNumberMobile = $userDetails?->getPhoneNumberMobile();
        $this->jobDescription = $userDetails?->getJobDescription();
        $this->address = $userDetails?->getAddress();
        $this->slackUserId = $userDetails?->getSlackUserId();
        $this->slackTeamId = $userDetails?->getSlackTeamId();
        $this->plz = $userDetails?->getPLZ();
        $this->city = $userDetails?->getCity();
        $this->country = $userDetails?->getCountry();
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     */
    public function setGender(?string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumberMobile(): ?string
    {
        return $this->phoneNumberMobile;
    }

    /**
     * @param string|null $phoneNumberMobile
     */
    public function setPhoneNumberMobile(?string $phoneNumberMobile): void
    {
        $this->phoneNumberMobile = $phoneNumberMobile;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getProfilePicture(): ?string
    {
        return $this->profilePicture;
    }

    /**
     * @param string $profilePicture
     */
    public function setProfilePicture(?string $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return string
     */
    public function getDigtUId(): ?int
    {
        return $this->digtUId;
    }

    /**
     * @param string $digtUId
     */
    public function setDigtUId(?int $digtUId): void
    {
        $this->digtUId = $digtUId;
    }

    /**
     * @return mixed
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @param $companies
     */
    public function setCompanies($companies): void
    {
        $this->companies = $companies;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getBirthDate(): ?string
    {
        return $this->birthDate;
    }

    public function setBirthDate(?string $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    public function getHireDate(): ?string
    {
        return $this->hireDate;
    }

    public function setHireDate(?string $hireDate): void
    {
        $this->hireDate = $hireDate;
    }

    public function getJobDescription(): ?string
    {
        return $this->jobDescription;
    }

    public function setJobDescription(?string $jobDescription): void
    {
        $this->jobDescription = $jobDescription;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getPLZ(): ?int
    {
        return $this->plz;
    }

    public function setPLZ(?int $plz): void
    {
        $this->plz = $plz;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getSlackUserId(): ?string
    {
        return $this->slackUserId;
    }

    public function setSlackUserId(?string $slackUserId): void
    {
        $this->slackUserId = $slackUserId;
    }

    public function getSlackTeamId(): ?string
    {
        return $this->slackTeamId;
    }

    public function setSlackTeamId(?string $slackTeamId): void
    {
        $this->slackTeamId = $slackTeamId;
    }

    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }
}
