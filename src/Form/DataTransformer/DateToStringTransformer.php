<?php

namespace App\Form\DataTransformer;

use Exception;
use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

class DateToStringTransformer implements DataTransformerInterface
{

    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        if (is_string($value)) {
            return [$value];
        }

        if (is_array($value)) {
            if (! isset($value[0])) {
                if (! isset($value["date"])) {
                    return [null];
                }

                return [$value["date"]];
            }

            return [$value[0]->format('d.m.Y')];
        }

        return [$value->format('d.m.Y')];
    }

    /**
     * @throws Exception
     */
    public function reverseTransform($value): ?DateTime
    {
        if (isset($value['date'])) {
            return new DateTime($value['date']);
        }

        return null;
    }
}