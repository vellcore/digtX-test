<?php

declare(strict_types=1);

namespace App\Entity\User;

use App\Entity\Country\Country;
use App\Entity\User;
use App\Form\Model\UserDetail;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_user_details")
 */
class UserDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="details")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ORM\Column(type="string", length=80, name="JobDescription", nullable=true)
     */
    private ?string $jobDescription;

    /**
     * @ORM\Column(type="string", length=100, name="Address", nullable=true)
     */
    private ?string $address;

    /**
     * @ORM\Column(type="integer", length=11, name="PLZ", nullable=true)
     */
    private ?int $plz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Country\Country", inversedBy="usersDetails")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private ?Country $country;

    /**
     * @ORM\Column(type="string", length=80, name="City", nullable=true)
     */
    private ?string $city;

    /**
     * @ORM\Column(type="string", length=20, name="PhoneNumber", nullable=true)
     */
    private ?string $phoneNumber;

    /**
     * @ORM\Column(type="string", length=20, name="PhoneNumberMobile", nullable=true)
     */
    private ?string $phoneNumberMobile;

    /**
     * @ORM\Column(type="string", length=40, name="SlackUserId", nullable=true)
     */
    private ?string $slackUserId;

    /**
     * @ORM\Column(type="string", length=40, name="SlackTeamId", nullable=true)
     */
    private ?string $slackTeamId;

    /**
     * @ORM\Column(type="datetime_immutable", name="BirthDate", nullable=true)
     */
    private ?\DateTimeImmutable $birthDate;

    /**
     * @ORM\Column(type="datetime_immutable", name="HireDate", nullable=true)
     */
    private ?\DateTimeImmutable $hireDate;

    public function __construct(User $user, UserDetail $userModel)
    {
        $user->setDetails($this);

        $this->user = $user;
        $this->update($userModel);
    }

    public function update(UserDetail $userModel): void
    {
        $this->jobDescription = $userModel->getJobDescription();
        $this->plz = $userModel->getPLZ();
        $this->address = $userModel->getAddress();
        $this->city = $userModel->getCity();
        $this->country = $userModel->getCountry();
        $this->phoneNumber = $userModel->getPhoneNumber();
        $this->phoneNumberMobile = $userModel->getPhoneNumberMobile();
        $this->slackUserId = $userModel->getSlackUserId();
        $this->slackTeamId = $userModel->getSlackTeamId();

        $birthDate = $userModel->getBirthDate()
            ? \DateTimeImmutable::createFromFormat('d.m.Y', $userModel->getBirthDate())
            : null;

        $this->birthDate = $birthDate;

        $hireDate = $userModel->getBirthDate()
            ? \DateTimeImmutable::createFromFormat('d.m.Y', $userModel->getHireDate())
            : null;

        $this->hireDate = $hireDate;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function getPhoneNumberMobile(): ?string
    {
        return $this->phoneNumberMobile;
    }

    public function getBirthDate(): ?\DateTimeImmutable
    {
        return $this->birthDate;
    }

    public function getHireDate(): ?\DateTimeImmutable
    {
        return $this->hireDate;
    }

    public function getJobDescription(): ?string
    {
        return $this->jobDescription;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getPLZ(): ?int
    {
        return $this->plz;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getSlackUserId(): ?string
    {
        return $this->slackUserId;
    }

    public function getSlackTeamId(): ?string
    {
        return $this->slackTeamId;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }
}