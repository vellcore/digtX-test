<?php

declare(strict_types=1);

namespace App\Entity\Country;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_countries")
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=80, nullable=false, name="NameEn")
     */
    private string $nameEn;

    /**
     * @ORM\Column(type="string", length=80, nullable=false, name="NameDe")
     */
    private string $nameDe;

    /**
     * @ORM\Column(type="string", length=5, nullable=false, name="ISO3166")
     */
    private string $iso3166;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User\UserDetails", mappedBy="country")
     */
    protected Collection $usersDetails;

    public function __construct(string $nameEn, string $nameDe, string $iso3166)
    {
        $this->nameEn = $nameEn;
        $this->nameDe = $nameDe;
        $this->iso3166 = $iso3166;

        $this->usersDetails = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNameEn(): string
    {
        return $this->nameEn;
    }

    public function __toString()
    {
        return 'country';
    }
}