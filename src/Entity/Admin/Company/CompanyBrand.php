<?php

namespace App\Entity\Admin\Company;

use App\Entity\Work\D04_Logistik\Invinit2\BrandSpecificationKey;
use App\Entity\Work\D04_Logistik\Invinit2\Product;
use App\Repository\Admin\Company\CompanyBrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyBrandRepository::class)
 * @ORM\Table(name = "app_invi_company_brand")
 */
class CompanyBrand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="BrandStatus")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="companyBrands")
     * @ORM\JoinColumn(nullable=false, name="fkCompany")
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, name="BrandName")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=600, nullable=true, name="BrandDescription")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=600, nullable=true, name="BrandLogo")
     */
    private $logo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
