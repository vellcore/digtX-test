<?php

namespace App\Entity\Admin\Company;

use App\Repository\Admin\Company\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @ORM\Table(name = "app_user_company")
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="UserCompanyStatus")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=100, name="UserCompanyName")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=600, nullable=true, name="UserCompanyLogo")
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=200, name="UserCompanyEmail")
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=CompanyEmployee::class, mappedBy="company")
     */
    private $companyEmployees;

    /**
     * @ORM\Column(type="string", length=40, name="UserCompanySlackTeam", nullable=true)
     */
    private ?string $slackTeamId = null;

    public function __construct()
    {
        $this->companyEmployees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|CompanyEmployee[]
     */
    public function getCompanyEmployees(): Collection
    {
        return $this->companyEmployees;
    }

    public function addCompanyEmployee(CompanyEmployee $companyEmployee): self
    {
        if (!$this->companyEmployees->contains($companyEmployee)) {
            $this->companyEmployees[] = $companyEmployee;
            $companyEmployee->setCompany($this);
        }

        return $this;
    }

    public function removeCompanyEmployee(CompanyEmployee $companyEmployee): self
    {
        if ($this->companyEmployees->removeElement($companyEmployee)) {
            // set the owning side to null (unless already changed)
            if ($companyEmployee->getCompany() === $this) {
                $companyEmployee->setCompany(null);
            }
        }

        return $this;
    }

    public function getSlackTeamId(): ?string
    {
        return $this->slackTeamId;
    }

    public function setSlackTeam(string $slackTeamId): void
    {
        $this->slackTeamId = $slackTeamId;
    }

    public function __toString()
    {
        return $this->name;
    }
}
