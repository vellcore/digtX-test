<?php

namespace App\Entity;

use App\Entity\Admin\Company\CompanyEmployee;
use App\Entity\User\UserDetails;
use App\Form\Model\UserDetail;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name = "app_user")
 * @method string getUserIdentifier()
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="UserStatus", options={"default" : 1})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=400, name="UserEmail")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=400, name="UserName")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=400, nullable=true, name="UserLastName")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=400, name="UserUsername")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=400, name="UserGender", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=400, name="UserProfilePicture", nullable=true)
     */
    private $profilePicture;

    /**
     * @ORM\OneToMany(targetEntity=CompanyEmployee::class, mappedBy="user", cascade={"persist"})
     */
    private $companies;

    /**
     * @ORM\Column(type="integer", length=255, name="UserDigtUId", nullable=true)
     */
    private $digtUId;

    /**
     * @ORM\Column(type="datetime", name="UserLastLogin", nullable=true)
     */
    private $lastLogin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\UserDetails", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private ?UserDetails $details = null;

    public function __construct()
    {
        $this->companies = new ArrayCollection();
    }

    public function update(UserDetail $userModel): void
    {
        $this->name = $userModel->getName();
        $this->lastName = $userModel->getLastName();
        $this->gender = $userModel->getGender();
        $this->status = $userModel->getStatus();
        $this->digtUId = $userModel->getDigtUId();
        $this->email = $userModel->getEmail();
        $this->username = $userModel->getEmail();

        $this->getDetails()
            ? $this->getDetails()->update($userModel)
            : new UserDetails($this, $userModel);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status): void
    {
        $this->status = $status;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    public function getProfilePicture()
    {
        return $this->profilePicture;
    }

    public function setProfilePicture($profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return Collection|CompanyEmployee[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function setCompanies($companies)
    {
        $this->companies = $companies;
    }

    public function addCompany(CompanyEmployee $company): self
    {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setUser($this);
        }

        return $this;
    }

    public function removeCompany(CompanyEmployee $company): self
    {
        if ($this->companies->removeElement($company)) {
            // set the owning side to null (unless already changed)
            if ($company->getUser() === $this) {
                $company->setUser(null);
            }
        }

        return $this;
    }

    public function getDigtUId()
    {
        return $this->digtUId;
    }

    public function setDigtUId($digtUId): void
    {
        $this->digtUId = $digtUId;
    }

    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function setLastLogin($lastLogin): void
    {
        $this->lastLogin = $lastLogin;
    }

    public function __toString()
    {
        return $this->username ?? "";
    }

    public function getDetails(): ?UserDetails
    {
        return $this->details;
    }

    public function setDetails(UserDetails $details): void
    {
        $this->details = $details;
    }
}
