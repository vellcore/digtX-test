<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class LogoutController extends AbstractController
{
    public function googleLogOut(Session $session): Response
    {
        $session->invalidate();
        return $this->redirect('https://accounts.google.com/Logout');
    }
}