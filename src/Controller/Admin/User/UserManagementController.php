<?php

namespace App\Controller\Admin\User;

use App\Entity\Admin\Company\CompanyEmployee;
use App\Entity\User;
use App\Entity\UserAuth;
use App\Form\Admin\User\Type\UserManagementCompanyType;
use App\Form\Admin\User\Type\UserManagementDetailType;
use App\Form\Admin\User\Type\UserManagementListType;
use App\Form\Admin\User\Type\UserManagementPermissionType;
use App\Form\Model\UserDetail;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManagementController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ValidatorInterface $validator;
    private FileUploader $fileUploader;
    private UserPasswordHasherInterface $userPasswordHasher;

    private ObjectRepository $userRepository;
    private ObjectRepository $userAuthRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        FileUploader $fileUploader,
        UserPasswordHasherInterface $userPasswordHasher,
    ) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->fileUploader = $fileUploader;
        $this->userPasswordHasher = $userPasswordHasher;

        $this->userRepository = $entityManager->getRepository(User::class);
        $this->userAuthRepository = $entityManager->getRepository(UserAuth::class);
    }

    public function showUserList(): Response
    {
        $list = $this->createForm(UserManagementListType::class);
        return $this->renderForm('Admin/User/user_management_list.html.twig', [
            'list' => $list,
        ]);
    }

    public function editUserDetail(int $userId, Request $request)
    {
        $user = $this->userRepository->find($userId);

        if ($user === null) {
            $user = new User();
        }

        $userModel = new UserDetail($user);

        $form = $this->createForm(UserManagementDetailType::class, $userModel);
        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted()) {
            $userAuth = $this->userAuthRepository->findOneBy(['systemUser' => $user]);

            if ($userAuth === null) {
                $userAuth = new UserAuth();
            }

            if (strlen($userModel->getPassword()) > 0) {
                $userAuth->setPassword(
                    $this->userPasswordHasher->hashPassword($userAuth, $userModel->getPassword())
                );
            }

            $errors = $this->validator->validate($userModel);
            $errors->addAll($this->validator->validate($userAuth));

            if (count($errors) === 0) {
                $user->update($userModel);

                $userAuth->setEmail($userModel->getEmail());
                $userAuth->setSystemUser($user);

                /** @var UploadedFile $file */
                $pb = $form->get('profilePicture')->getData();

                if ($pb) {
                    $fileName = $this->fileUploader->uploadUserProfilePicture($pb);
                    $user->setProfilePicture($fileName);
                }

                $this->entityManager->persist($user);
                $this->entityManager->persist($userAuth);
                $this->entityManager->flush();

                $this->addFlash('success', "User \"{$user->getEmail()}\" successfully saved!");

                if ($request->request->get('user-detail') !== null) {
                    return $this->redirectToRoute(
                        'app_admin_user_management_edit_detail',
                        ['userId' => $user->getId()]
                    );
                } elseif ($request->request->get('submit') !== null) {
                    return $this->redirectToRoute(
                        'app_admin_user_management_edit_detail',
                        ['userId' => $user->getId()]
                    );
                }

                return $this->redirectToRoute('app_admin_user_management_list');
            }
        }

        foreach ($errors as $error) {
            $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage()));
        }

        return $this->renderForm('Admin/User/user_management_edit_detail.html.twig', [
            'form' => $form,
            'userId' => $userId,
        ]);
    }
}
