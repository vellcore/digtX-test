<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Model\UserDetail;
use App\Form\Type\UserDetailForm;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Security $security;
    private ValidatorInterface $validator;

    private ObjectRepository $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        Security $security,
        ValidatorInterface $validator,
    ) {
        $this->entityManager = $entityManager;
        $this->security = $security;
        $this->validator = $validator;

        $this->userRepository = $this->entityManager->getRepository(User::class);
    }

    public function getDropdownUserList(Request $request): Response
    {
        $search = $request->query->get('q');
        $users = $this->userRepository->getUsers($search);

        $result = "";
        foreach ($users as $wa) {
            $result .= '<li class="list-group-item" role="option" data-autocomplete-value="'
                . $wa['id']
                . '">' . $wa['username'] . '</li>';
        }

        return new Response($result);
    }

    public function changeUserDetails(Request $request): Response
    {
        /** @var User $user */
        $user = $this->security->getUser()->getSystemUser();

        $userModel = new UserDetail($user);

        $form = $this->createForm(UserDetailForm::class, $userModel);
        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted()) {
            $errors = $this->validator->validate($userModel);

            if ($form->isValid()) {
                $user->update($userModel);

                /** @var UploadedFile $file */
                $pb = $form->get('profilePicture')->getData();

                if ($pb) {
                    // ...
                }

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_user-detail');
            }
        }

        foreach ($errors as $error) {
            $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage()));
        }

        return $this->renderForm('registration/user_details.html.twig', [
            'form' => $form,
            'userId' => $user->getId(),
        ]);
    }

    public function getModalContent()
    {
        return new Response("Content");
    }
}