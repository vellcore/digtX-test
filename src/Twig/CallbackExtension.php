<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CallbackExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('callback', [$this, 'executeCallback']),
        ];
    }

    public function executeCallback($callback, $arguments)
    {
        return $callback($arguments);
    }
}
