<?php

declare(strict_types=1);

namespace App\Service\Company\Manager;

use App\Entity\Admin\Company\Company;
use Doctrine\ORM\EntityManagerInterface;

class CompanyManager
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getSlackTeamsList(): array
    {
        $list = [];

        $companies = $this->em->getRepository(Company::class)->findAll();

        /** @var Company $company */
        foreach ($companies as $company) {
            $list[$company->getSlackTeamId()] = $company->getId();
        }

        return $list;
    }
}