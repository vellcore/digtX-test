<?php

namespace App\Service;

use App\Form\Type\DatePickerFilterType;
use App\Form\Type\DropdownFilterType;
use App\Form\Type\MultiselectFilterType;
use App\Service\Helper\ArrayHelper;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Symfony\Component\Form\Form;

class ListViewPaginator
{
    private array $data;
    private int $pageCount = 1;
    private int $currentPage = 1;

    private array $values = [];

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function getPageCount(): int
    {
        return $this->pageCount;
    }

    public function setPageCount(int $pageCount): void
    {
        $this->pageCount = $pageCount;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    public function build(QueryBuilder $queryBuilder, array $params, array $options, Form $form): static
    {
        $queryBuilder = $this->applyFilters($queryBuilder, $params, $options, $form);
        $queryBuilder = $this->applySearch($queryBuilder, $params, $options, $form);

        $paginator = new Paginator($queryBuilder);

        $pageSize = $params[$form->getName() . '_list_view_page_limiter'];

        $totalItems = count($paginator);
        $pageCount = (int) ceil($totalItems / $pageSize);
        $currentPage = (int) ($params[$form->getName() . '_list_view_current_page'] ?? 1);

        if ($currentPage > $pageCount) {
            $currentPage = $pageCount;
        }

        if ($currentPage <= 0) {
            $currentPage = 1;
        }

        if ($pageCount <= 0) {
            $pageCount = 1;
        }

        $this->setData($paginator->getQuery()
            ->setFirstResult($pageSize * ($currentPage - 1))
            ->setMaxResults($pageSize)
            ->getArrayResult());

        $this->setPageCount($pageCount);
        $this->setCurrentPage($currentPage);

        return $this;
    }

    /**
     * @throws \Exception
     */
    private function applyFilters(QueryBuilder $queryBuilder, array $params, array $options, $form): QueryBuilder
    {
        $filters = $options['filters'] ?? [];

        foreach ($params as $key => $param) {
            // skip irrelevant params
            if (str_starts_with($key, $form->getName() . '_list_view') || ! is_array($param)) {
                continue;
            }

            // fetch values from FormTypes
            $havings = [];
            foreach ($filters as $filter) {
                $submittedFilter = array_column($param, $filter['name']);

                if ($filter['type'] === DropdownFilterType::class) {
                    $filterValues = ArrayHelper::flattenArray($submittedFilter);

                    // remove alias prefix
                    $parameter = preg_replace("/\w+\./", "", $filter['field']);

                    foreach ($filterValues as $filterValue) {
                        if ($filterValue === "" || $filterValue === null) {
                            continue;
                        }

                        $havings[] = "({$filter['field']} = :$parameter OR {$filter['field']} LIKE :{$parameter}Like)";
                        $queryBuilder->setParameter($parameter, $filterValue);
                        $queryBuilder->setParameter("{$parameter}Like", "%$filterValue%");
                    }
                } elseif ($filter['type'] === DatePickerFilterType::class) {
                    $filterValues = ArrayHelper::flattenArray($submittedFilter);

                    // remove alias prefix
                    $parameter = preg_replace("/\w+\./", "", $filter['field']);

                    $from = (DateTime::createFromFormat('d.m.Y', "01.01.1970"))->setTimezone(
                        new DateTimeZone('Europe/Zurich')
                    );
                    if (! empty($filterValues["0_datepickerFilterFrom_date"])) {
                        $from = (DateTime::createFromFormat(
                            'd.m.Y',
                            $filterValues["0_datepickerFilterFrom_date"]
                        ))->setTimezone(
                            new DateTimeZone('Europe/Zurich')
                        );
                    }

                    $to = (new DateTime())->setTimezone(
                        new DateTimeZone('Europe/Zurich')
                    );

                    if (! empty($filterValues["0_datepickerFilterTo_date"])) {
                        $to = (DateTime::createFromFormat(
                            'd.m.Y',
                            $filterValues["0_datepickerFilterTo_date"]
                        ))->setTimezone(
                            new DateTimeZone('Europe/Zurich')
                        );
                    }

                    $whereString = " {$filter['field']} BETWEEN :{$parameter}From AND :{$parameter}To ";
                    $queryBuilder->setParameter($parameter . "From", $from);
                    $queryBuilder->setParameter($parameter . "To", $to);

                    $queryBuilder->andWhere($whereString);
                } elseif ($filter['type'] === MultiselectFilterType::class) {
                    $filterValues = ArrayHelper::flattenArray($submittedFilter);

                    // remove alias prefix
                    $parameter = preg_replace("/\w+\./", "", $filter['field']);

                    foreach ($filterValues as $filterValue) {
                        if ($filterValue === "" || $filterValue === null) {
                            continue;
                        }
                        $havings[] = "({$filter['field']} IN(:$parameter)"
                            . "OR {$filter['field']} LIKE :{$parameter}Like)";

                        $queryBuilder->setParameter("{$parameter}Like", "%$filterValue%");

                        $filterValue = explode(",", $filterValue);
                        $queryBuilder->setParameter($parameter, $filterValue);
                    }
                }
            }

            if (count($havings) > 0) {
                $queryBuilder->having(implode(" AND ", $havings));
            }
        }

        return $queryBuilder;
    }

    private function applySearch(QueryBuilder $queryBuilder, array $params, array $options, $form): QueryBuilder
    {
        $search = empty($params[$form->getName() . '_list_view_search'])
            ? null
            : $params[$form->getName() . '_list_view_search'];
        $searchFields = $options['search_fields'];

        $havings = [];
        foreach ($searchFields as $searchField) {
            $havings[] = "$searchField LIKE :search";
        }

        if (count($havings) > 0 && $search !== null) {
            $queryBuilder->andHaving(implode(" OR ", $havings));
            $queryBuilder->setParameter('search', "%$search%");
        }

        return $queryBuilder;
    }
}
