<?php

namespace App\Repository\Admin\Company;

use App\Entity\Admin\Company\Company;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method \App\Entity\Admin\Company\CompanyBrand|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Entity\Admin\Company\CompanyBrand|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Entity\Admin\Company\CompanyBrand[]    findAll()
 * @method \App\Entity\Admin\Company\CompanyBrand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyBrandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, \App\Entity\Admin\Company\CompanyBrand::class);
    }

    public function findByUser(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('b')
            ->join('App\Entity\Admin\Company\Company', 'c')
            ->join('App\Entity\Admin\Company\CompanyEmployee', 'ce')
            ->andWhere('b.company = c')
            ->andWhere('ce.company = c')
            ->andWhere('ce.user = :user')
            ->andWhere('c.status = 1')
            ->setParameter('user', $user);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getCompanyBrandListQueryBuilder($company)
    {
        $qb = $this->createQueryBuilder('cb');

        $qb
            ->addSelect('cb.id')
            ->addSelect('cb.status')
            ->addSelect('cb.name')
            ->addSelect('cb.description')
            ->addSelect('c.id as companyId')
            ->join(
                Company::class,
                "c",
                Join::WITH,
                "cb.company = c"
            )
            ->andWhere("cb.company = :company")
            ->setParameter("company", $company);

        return $qb;
    }
}
