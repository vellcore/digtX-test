<?php

namespace App\Repository\Admin\Company;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method \App\Entity\Admin\Company\Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method \App\Entity\Admin\Company\Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method \App\Entity\Admin\Company\Company[]    findAll()
 * @method \App\Entity\Admin\Company\Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, \App\Entity\Admin\Company\Company::class);
    }

    public function findByUser(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->join('App\Entity\Admin\Company\CompanyEmployee', 'ce')
            ->andWhere('ce.company = c')
            ->andWhere('ce.user = :user')
            ->andWhere('c.status = 1')
            ->setParameter('user', $user);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getCompanies($search): array
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('c.id', 'c.name')
            ->andWhere("c.name LIKE :search ")
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }

    public function getCompaniesByUser($search, User $user): array
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select('c.id', 'c.name')
            ->join("App\Entity\User", "u")
            ->join("App\Entity\Admin\Company\CompanyEmployee", "ce")
            ->andWhere("ce.user = u")
            ->andWhere("ce.company = c")
            ->andWhere("c.name LIKE :search ")
            ->andWhere("u = :user ")
            ->setParameter('user', $user)
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }

    public function getCompanyManagementList()
    {
        return $this->createQueryBuilder('c');
    }
}

