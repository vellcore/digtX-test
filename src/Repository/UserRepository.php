<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsers($search): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('u.id', 'u.username')
            ->andWhere("u.username LIKE :search OR u.name LIKE :search OR u.lastName LIKE :search")
            ->setParameter('search', "%$search%")
            ->getQuery();

        return $queryBuilder->getArrayResult();
    }

    public function getUserList(): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->select('u');
    }

    public function getUserManagementList()
    {
        $qb = $this->createQueryBuilder('u');

        $qb
            ->addSelect(
                'u.id',
                'u.name',
                'u.status',
                'u.lastName',
                'u.email',
                'u.digtUId',
            )
            ->leftJoin(
                'App\Entity\Admin\Company\CompanyEmployee',
                'ce',
                Join::WITH,
                'ce.user = u',
            )
            ->leftJoin(
                'App\Entity\Admin\Company\Company',
                'c',
                Join::WITH,
                'ce.company = c'
            )
            ->groupBy('u.id', 'u.lastName', 'u.email');

        return $qb;
    }
}
