// assets/controllers/list-view-controller.js
import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static targets = [
        "turboFrame",
    ];

    searchDelayTimer;

    connect()
    {
        // autofocus on search bar and add cursor after text
        let searchInput = this.element.querySelectorAll('.search-bar .search');

        if (searchInput[0]) {
            searchInput[0].focus();
            let text = searchInput[0].value;
            searchInput[0].value = '';
            searchInput[0].value = text;
        }
    }

    prev()
    {
        const currentPage = $("#" + this.turboFrameTarget.id + " #list_view_current_page");
        currentPage[0].value = parseInt(currentPage[0].value) - 1;
    }

    next()
    {
        const currentPage = $("#" + this.turboFrameTarget.id + " #list_view_current_page");
        currentPage[0].value = parseInt(currentPage[0].value) + 1;
    }

    async submit()
    {
        const $form = $("#" + this.turboFrameTarget.id + " form");
        $form[0].requestSubmit();
    }

    autoSubmit(event)
    {
        this.submit();
    }

    searchTrigger(event)
    {
        const node = this;
        clearTimeout(this.searchDelayTimer);
        this.searchDelayTimer = setTimeout(function () {
            node.autoSubmit(event);
        }, 500)
    }
}