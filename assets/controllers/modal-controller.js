import { Controller } from 'stimulus';
import { Modal } from 'bootstrap';

export default class extends Controller {
    static targets = ['modal', 'modalBody'];
    static values = {
        formUrl: String,
    }

    async openModal(event) {
        const modal = new Modal(this.modalTarget);
        modal.show();

       await this.loadModalData();
    }

    loadModalData() {
        let scope = this;
        return new Promise(async function (resolve, reject) {
            scope.modalBodyTarget.innerHTML = "Loading...";

            let response = await fetch(scope.formUrlValue);
            scope.modalBodyTarget.innerHTML = await response.text();

            resolve(response);
        });
    }
}