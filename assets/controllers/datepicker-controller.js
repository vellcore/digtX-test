// assets/controllers/datepicker-controller.js
import { Controller } from '@hotwired/stimulus';
import {useDispatch} from "stimulus-use";

/* stimulusFetch: 'lazy' */
export default class extends Controller {

    connect()
    {
        useDispatch(this);

        $(".datepicker").datepicker({
            defaultDate: "+5",
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
        })

        let scope = this;
        $(this.element).find(".datepicker").change(function (event) {
            scope.onChange(event);
        });
    }

    onChange(event)
    {
        this.dispatch('change');
    }

    input(event)
    {
        if (event.currentTarget.value !== "") {
            return;
        }

        $(".datepicker").datepicker("setDate", null).change();
    }
}