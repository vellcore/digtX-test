import { Controller } from '@hotwired/stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    connect()
    {
        let element = this.element;

        element.getElementsByClassName('dx-accordion-header')[0].addEventListener(
            'click',
            function (event) {
                let accordion = element;
                let expanded = accordion.getAttribute('aria-expanded') == "true" ? "false" : "true";

                accordion.setAttribute('aria-expanded', expanded);
        }.bind(this), false)
    }
}