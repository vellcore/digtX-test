import { Modal } from 'bootstrap';

const TurboHelper = class {
    constructor()
    {
        document.addEventListener('turbo:submit-start', (event) => {
            const submitter = event.detail.formSubmission.submitter;
            if (submitter) {
                submitter.toggleAttribute('disabled', true);
                submitter.classList.add('turbo-submit-disabled');
            }
        })

        document.addEventListener('turbo:before-cache', () => {
            if (document.body.classList.contains('modal-open')) {
                const modalEl = document.querySelector('.modal');
                const modal = Modal.getInstance(modalEl);
                modalEl.classList.remove('fade');
                modal._backdrop._config.isAnimated = false;
                modal.hide();
                modal.dispose();

            }
            // internal way to see if sweetalert2 has been imported yet
            if (__webpack_modules__[require.resolveWeak('sweetalert2')]) {
                // because we know it's been imported, this will run synchronously
                import(/* webpackMode: 'weak' */'sweetalert2').then((Swal) => {
                    if (Swal.default.isVisible()) {
                        Swal.default.getPopup().style.animationDuration = '0ms'
                        Swal.default.close();
                    }
                })
            }

            this.reenableSubmitButtons();
        });
    }

    reenableSubmitButtons() {
        document.querySelectorAll('.turbo-submit-disabled').forEach((button) => {
            button.toggleAttribute('disabled', false);
            button.classList.remove('turbo-submit-disabled');
        });
    }
}
export default new TurboHelper();