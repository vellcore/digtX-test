
import './styles/login/corporate.css';
import './styles/app.scss';
import './styles/style.css';
import './styles/responsive.css';
import './styles/digtX/list-view-renderer.css';
import './styles/digtX/turbo.css';
import './styles/digtX/accordion.scss';
import './styles/digtX/tom-select.scss';

// start the Stimulus application
import './bootstrap';
import './turbo/turbo-helper';
