<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220730112359 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user_company CHANGE UserCompanySlackTeam UserCompanySlackTeam VARCHAR(40) DEFAULT NULL');
        $this->addSql('ALTER TABLE app_user_details CHANGE JobDescription JobDescription VARCHAR(80) DEFAULT NULL, CHANGE Address Address VARCHAR(100) DEFAULT NULL, CHANGE PLZ PLZ INT DEFAULT NULL, CHANGE City City VARCHAR(80) DEFAULT NULL, CHANGE PhoneNumber PhoneNumber VARCHAR(20) DEFAULT NULL, CHANGE PhoneNumberMobile PhoneNumberMobile VARCHAR(20) DEFAULT NULL, CHANGE SlackUserId SlackUserId VARCHAR(40) DEFAULT NULL, CHANGE SlackTeamId SlackTeamId VARCHAR(40) DEFAULT NULL, CHANGE BirthDate BirthDate DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE HireDate HireDate DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user_company CHANGE UserCompanySlackTeam UserCompanySlackTeam VARCHAR(40) NOT NULL');
        $this->addSql('ALTER TABLE app_user_details CHANGE JobDescription JobDescription VARCHAR(80) NOT NULL, CHANGE Address Address VARCHAR(100) NOT NULL, CHANGE PLZ PLZ INT NOT NULL, CHANGE City City VARCHAR(80) NOT NULL, CHANGE PhoneNumber PhoneNumber VARCHAR(20) NOT NULL, CHANGE PhoneNumberMobile PhoneNumberMobile VARCHAR(20) NOT NULL, CHANGE SlackUserId SlackUserId VARCHAR(40) NOT NULL, CHANGE SlackTeamId SlackTeamId VARCHAR(40) NOT NULL, CHANGE BirthDate BirthDate DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE HireDate HireDate DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }
}
