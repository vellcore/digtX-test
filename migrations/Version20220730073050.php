<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220730073050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_countries (id INT AUTO_INCREMENT NOT NULL, NameEn VARCHAR(80) NOT NULL, NameDe VARCHAR(80) NOT NULL, ISO3166 VARCHAR(5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_details (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, country_id INT DEFAULT NULL, JobDescription VARCHAR(80) NOT NULL, Address VARCHAR(100) NOT NULL, PLZ INT NOT NULL, City VARCHAR(80) NOT NULL, PhoneNumber VARCHAR(20) NOT NULL, PhoneNumberMobile VARCHAR(20) NOT NULL, SlackUserId VARCHAR(40) NOT NULL, SlackTeamId VARCHAR(40) NOT NULL, BirthDate DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', HireDate DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_E8A50F01A76ED395 (user_id), INDEX IDX_E8A50F01F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_user_details ADD CONSTRAINT FK_E8A50F01A76ED395 FOREIGN KEY (user_id) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_user_details ADD CONSTRAINT FK_E8A50F01F92F3E70 FOREIGN KEY (country_id) REFERENCES app_countries (id)');
        $this->addSql('ALTER TABLE app_user DROP UserPhoneNumber, DROP UserPhoneNumberMobile');

        // Setting up of default value can be helpful if there existing data, so thats the example
        $this->addSql('ALTER TABLE app_user_company ADD UserCompanySlackTeam VARCHAR(40) NOT NULL DEFAULT "T01HPFCP0RG"');
        $this->addSql('ALTER TABLE app_user_company ALTER UserCompanySlackTeam DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user_details DROP FOREIGN KEY FK_E8A50F01F92F3E70');
        $this->addSql('DROP TABLE app_countries');
        $this->addSql('DROP TABLE app_user_details');
        $this->addSql('ALTER TABLE app_user ADD UserPhoneNumber VARCHAR(255) DEFAULT NULL, ADD UserPhoneNumberMobile VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE app_user_company DROP UserCompanySlackTeam');
    }
}
