<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220730062405 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_invi_company_brand (id INT AUTO_INCREMENT NOT NULL, BrandStatus INT NOT NULL, BrandName VARCHAR(255) NOT NULL, BrandDescription VARCHAR(600) DEFAULT NULL, BrandLogo VARCHAR(600) DEFAULT NULL, fkCompany INT NOT NULL, INDEX IDX_B7F091B5599FBFC0 (fkCompany), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user (id INT AUTO_INCREMENT NOT NULL, UserStatus INT DEFAULT 1 NOT NULL, UserEmail VARCHAR(400) NOT NULL, UserName VARCHAR(400) NOT NULL, UserLastName VARCHAR(400) DEFAULT NULL, UserUsername VARCHAR(400) NOT NULL, UserGender VARCHAR(400) DEFAULT NULL, UserProfilePicture VARCHAR(400) DEFAULT NULL, UserPhoneNumber VARCHAR(255) DEFAULT NULL, UserPhoneNumberMobile VARCHAR(255) DEFAULT NULL, UserDigtUId INT DEFAULT NULL, UserLastLogin DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_auth (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, googleAuthenticatorSecret VARCHAR(255) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, fkUser INT NOT NULL, UNIQUE INDEX UNIQ_2A30A3A5E7927C74 (email), UNIQUE INDEX UNIQ_2A30A3A58D68ADD3 (fkUser), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_company (id INT AUTO_INCREMENT NOT NULL, UserCompanyStatus INT NOT NULL, UserCompanyName VARCHAR(100) NOT NULL, UserCompanyLogo VARCHAR(600) DEFAULT NULL, UserCompanyEmail VARCHAR(200) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_company_employee (id INT AUTO_INCREMENT NOT NULL, fkCompany INT NOT NULL, fkUser INT NOT NULL, INDEX IDX_204135EE599FBFC0 (fkCompany), INDEX IDX_204135EE8D68ADD3 (fkUser), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('ALTER TABLE app_invi_company_brand ADD CONSTRAINT FK_B7F091B5599FBFC0 FOREIGN KEY (fkCompany) REFERENCES app_user_company (id)');
        $this->addSql('ALTER TABLE app_user_auth ADD CONSTRAINT FK_2A30A3A58D68ADD3 FOREIGN KEY (fkUser) REFERENCES app_user (id)');
        $this->addSql('ALTER TABLE app_user_company_employee ADD CONSTRAINT FK_204135EE599FBFC0 FOREIGN KEY (fkCompany) REFERENCES app_user_company (id)');
        $this->addSql('ALTER TABLE app_user_company_employee ADD CONSTRAINT FK_204135EE8D68ADD3 FOREIGN KEY (fkUser) REFERENCES app_user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE app_user_auth DROP FOREIGN KEY FK_2A30A3A58D68ADD3');
        $this->addSql('ALTER TABLE app_user_company_employee DROP FOREIGN KEY FK_204135EE8D68ADD3');
        $this->addSql('ALTER TABLE app_invi_company_brand DROP FOREIGN KEY FK_B7F091B5599FBFC0');
        $this->addSql('ALTER TABLE app_user_company_employee DROP FOREIGN KEY FK_204135EE599FBFC0');
        $this->addSql('DROP TABLE app_invi_company_brand');
        $this->addSql('DROP TABLE app_user');
        $this->addSql('DROP TABLE app_user_auth');
        $this->addSql('DROP TABLE app_user_company');
        $this->addSql('DROP TABLE app_user_company_employee');
        $this->addSql('DROP TABLE ext_log_entries');
    }
}
